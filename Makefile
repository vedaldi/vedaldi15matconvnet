.PHONY: post, clean

base=vedaldi15matconvnet
post:
	pdflatex $(base)
	bibtex $(base)
	pdflatex $(base)
	pdflatex $(base)
	rm -rf pack
	mkdir -p pack
	gs  -sDEVICE=pdfwrite -q -dBATCH -dNOPAUSE -dSAFER \
	    -dPDFSETTINGS=/prepress \
	    -dCompatibilityLevel=1.4 \
	    -dAutoFilterColorImages=false \
	    -dAutoFilterGrayImages=false \
	    -sOutputFile=pack/$(base).pdf \
	    -c `> setdistillerparams` \
	    -f $(base).pdf \
	    -c quit
	cp -f $(base).{pdf,tex,bbl} pack/
	cp -f sig-alternate-2013.cls pack/
	cp -rf figures pack/
	zip -r $(base)-pack.zip pack

clean:
	rm -f *.{blg,aux,log,out}
	rm -f setdistillerparams
	find . -name '*~' -delete
